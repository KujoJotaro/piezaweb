import React from "react"
import {
    Modal,
    ModalHeader,
    ModalBody,
    Form,
    FormGroup,
    Input,
    Label,
    Button, ButtonGroup
} from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons'

export default class CustomModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeItem: this.props.activeItem,
            cSelected: []
        };

        this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
        this.onCheckboxBtnClick = this.onCheckboxBtnClick.bind(this);
    }

    //  handleChange = e => {
    // let { name, value } = e.target;
    // const activeItem = { ...this.state.activeItem, [name]: value };
    // this.setState({ activeItem });
    // };


    onRadioBtnClick(rSelected) {
        this.setState({ rSelected });
    }

    onCheckboxBtnClick(selected) {
        const index = this.state.cSelected.indexOf(selected);
        if (index < 0) {
            this.state.cSelected.push(selected);
        } else {
            this.state.cSelected.splice(index, 1);
        }
        this.setState({ cSelected: [...this.state.cSelected] });
    }

    render() {
        const {toggle} = this.props;
        const item = this.props.activeItem;
        const {autor,tipo,saga} = item;
        let val = true;
        let lel = "No";
        if(item.leido)
            lel = "Si";
        return (
            <Modal isOpen={val} toggle={toggle} size="lg" centered={true}>
                <ModalHeader  toggle={toggle}>{item.titulo_libro}<small className="pb-0  mb-0"> {autor.nombre}</small></ModalHeader>

                <ModalBody>
                    <div className="row">
                        <div className="col-lg-6">
                            <Form>
                                <FormGroup>
                                    <Label for="title">Titulo: </Label>
                                    <Input
                                        type="text"
                                        name="title"
                                        value={item.titulo_libro}
                                        readOnly
                                    />
                                </FormGroup>

                                <FormGroup>
                                    <Label for="title">Autor: </Label>
                                    <Input
                                        type="text"
                                        name="title"
                                        value={autor.nombre}
                                        readOnly
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="title">Tipo: </Label>
                                    <Input
                                        type="text"
                                        name="title"
                                        value={tipo.nombre_tipo}
                                        readOnly
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="title">Saga: </Label>
                                    <Input
                                        type="text"
                                        name="title"
                                        value={saga === null ? "" : saga.titulo_saga}
                                        readOnly
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="title">Notas: </Label>
                                    <Input
                                        type="text"
                                        name="title"
                                        value={item.notas}
                                        readOnly
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="title">Leido: </Label>
                                    <ButtonGroup className="ml-2">
                                        <Button color="primary" outline readOnly>{lel}</Button>
                                    </ButtonGroup>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="title">Portada: <a rel="noopener noreferrer" href={item.portada} target="_blank"><FontAwesomeIcon icon={faExternalLinkAlt} /></a></Label>
                                    <Input
                                        type="text"
                                        name="title"
                                        value={item.portada}
                                        readOnly
                                    />
                                </FormGroup>
                            </Form>
                        </div>
                        <div className="col-lg-6 border-left ">
                            <img className="img-fluid w-100"
                                 alt="404"
                                 src={item.portada}/>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        );
    }
}


