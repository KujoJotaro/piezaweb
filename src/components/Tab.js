import React from "react"

function Tab(props) {
    const item = props.item;
       return(
           <div className="col-md-4 col-lg-3 p-2" key={item.key} onClick={() => {props.onClick(item)}}>
               <a href="#detalles" role="button" data-toggle="modal">
                   <img
                    alt="./404.png"
                    className="img-fluid image"
                    src={item.portada}
                   />
               </a>
           </div>
       );
}
export default Tab;