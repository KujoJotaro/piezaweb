import React, {Component} from 'react';
import axios from "axios"
import Modal from "./components/Modal";
import SearchBar from "./components/SearchBar"
import './App.css';


class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            modal: false,
            loading:false,
            data : {},
            activeItem: {},
            apiUrl:"http://192.168.1.97:1292/api/libros?ordering=titulo_libro",
            ordering:""
        };
        this.coverList = this.coverList.bind(this)
        this.tabList = this.tabList.bind(this)
    }

    tabList(item, i){
        if (item)
            return(
                <div className="col-md-4 col-lg-3 p-2" key={i} onClick={() => this.viewItem(item)}>
                        <img
                            alt="./404.png"
                            className="img-fluid image grow"
                            src={item.portada}
                        />
                </div>
            );
    }

    componentDidMount() {
        this.refreshList();
    }

    refreshList = (ord = "") => {
        console.log(ord);
        let api = this.state.apiUrl;
         api += ord === "" ? "" : ","+ord;
        this.setState({loading:true, ordering: ord})
        axios.get(api)
            .then(response => {this.setState({data: response.data, loading:false})})
            .catch(error => {
                console.log(error.response)
            });
    }

    coverList(){
        const items = this.state.data;
        let render = [];
        if (items.autor !== null)
            render =  Object.keys(items).map((item, i) => this.tabList(items[item], i));
        return(
            <div className="row p-md-3 mt-md-3">
                {render}
            </div>
        )
    }

    viewItem = item => {
        this.setState({ activeItem: item, modal: !this.state.modal });
    };

    toggle = () => {
        this.setState({ modal: !this.state.modal });
    };

    render() {
        const content = this.coverList();
        const spin = <div className="spinner-border" role="status"><span className="sr-only">Cargando...</span> </div>;
        return (
            <div className="App">
                <h1 className="display-1 text-center">Pieza Web</h1>

                <div className="container shadow-lg p-3 mb-5 bg-white rounde rounded ">
                    <SearchBar fos={this.refreshList}/>
                    <hr className="w-100"/>

                    {this.state.loading ? spin : content}
                </div>
                {this.state.modal ? (
                    <Modal
                        activeItem={this.state.activeItem}
                        toggle={this.toggle}
                    />
                ) : null}
            </div>
        );
    }
}

export default App;
