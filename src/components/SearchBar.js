import React from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

import {
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    Input,
    Button,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
}
    from "reactstrap"

class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdownOpen: false,
            ordering:""
        };

        this.toggle = this.toggle.bind(this);
        this.selectOrder = this.selectOrder.bind(this)
    }

    selectOrder(event){
        const {value} = event.target;
        if(this.state.ordering !== value){
            this.setState({ordering:value})
            this.props.fos(value);
        }
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    render() {
        return (
            <div className="row clear-fix sticky-top p-2 bg-white">
                <div className="col-12 col-md-8 offset-md-2">
                    <InputGroup className="">
                        <InputGroupButtonDropdown addonType="prepend" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                            <DropdownToggle className="text-capitalize" caret>
                                {this.state.ordering === "" ? "Filtros" : this.state.ordering}
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem value="autor" onClick={this.selectOrder}>Autor</DropdownItem>
                                <DropdownItem value="saga" onClick={this.selectOrder}>Saga</DropdownItem>
                                <DropdownItem value="tipo" onClick={this.selectOrder}>Tipo</DropdownItem>
                            </DropdownMenu>
                        </InputGroupButtonDropdown>
                        <Input />
                        <InputGroupAddon addonType="append">
                            <Button><FontAwesomeIcon icon={faSearch} /></Button>
                        </InputGroupAddon>
                    </InputGroup>


                </div>
            </div>
        );
    }
}

export default SearchBar